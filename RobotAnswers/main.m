//
//  main.m
//  RobotAnswers
//
//  Created by Art on 2015-05-15.
//  Copyright (c) 2015 piv. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
