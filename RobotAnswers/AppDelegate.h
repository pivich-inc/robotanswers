//
//  AppDelegate.h
//  RobotAnswers
//
//  Created by Art on 2015-05-15.
//  Copyright (c) 2015 piv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

